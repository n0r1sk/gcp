[[_TOC_]]

# Deploy a Service Mesh with Istio OSS on GKE private cluster

## Description
This demo shows how you can create and use not mutually exclusive multiple Istio gateways (ingress) for different purposes at the same time. Details about it can be found here -> https://gitlab.com/n0r1sk/gcp

![](multiple-istio-gateways.png)

The following steps can be automated inside a script, but to make it more transparent, the plain `gcloud` and `kubernetes` commands are shown here.

# Prepare for a privte GKE cluster

## Delete all existing default networks
After a new GCP project is created, all of the default created networks should be deleted for safety reasons. To delete them, the firewall-rules has to be deleted first. The deletion of a VPC with the GCP console UI is easy because the UI will take care about the correct sequence. To delete an entire VPC (the default one in this case), all firewall-rules has to be delete first.

~~~bash
for rule in $(gcloud compute firewall-rules list --format="value(Name)"); do gcloud -q compute firewall-rules delete $rule; done #delete all firewall rules

gcloud -q compute networks delete default #delete default vpc
~~~

## Create a private network

~~~bash
# specify the subnet range to use
# DO NOT USE THIS SUBNET AS IT IS ALREADY TAKEN!
GCP_SUBNET=
GCP_PRIVATE_VPN_CIDR=
GCP_REGION=europe-west3
GCP_PROJECT=k8s-cicd-1
 
# create the VPC
gcloud compute networks create hub-private-vpc --subnet-mode=custom --region=$GCP_REGION
 
# create the subnet
gcloud compute networks subnets create private-subnet --network=hub-private-vpc --region=$GCP_REGION --range=$GCP_SUBNET --enable-private-ip-google-access
 
# create basic firewall-rule set
gcloud compute firewall-rules create allow-private --direction=INGRESS --priority=1000 --network=hub-private-vpc --action=ALLOW --rules=all --source-ranges=10.200.0.0/16
gcloud compute firewall-rules create allow-inside --direction=INGRESS --priority=1000 --network=hub-private-vpc --action=ALLOW --rules=all --source-ranges=$GCP_SUBNET
gcloud compute firewall-rules create allow-hub --direction=INGRESS --priority=1000 --network=hub-private-vpc --action=ALLOW --rules=all --source-ranges=$GCP_PRIVATE_VPN_CIDR
gcloud compute firewall-rules create allow-google-clouddns --direction=INGRESS --priority=1000 --network=hub-private-vpc --action=ALLOW --rules=all --source-ranges=35.199.192.0/19
gcloud compute firewall-rules create allow-google-healthcheck-and-loadbalancer --direction=INGRESS --priority=1000 --network=hub-private-vpc --action=ALLOW --rules=all --source-ranges=130.211.0.0/22,35.191.0.0/16
gcloud compute firewall-rules create allow-google-web-ssh --direction=INGRESS --priority=1000 --network=hub-private-vpc --action=ALLOW --rules=all --source-ranges=35.235.240.0/20
~~~

## Create network peering

Now, create the network peering. There are a lot of different ways about how to do this. Therefore, it is assumend that all network related stuff like DNS, routing, etc. is working at this point.

## Create firewall rules

Because all firewall rules were deleted, it is needed that you setup new ones.

# Create a private GKE cluster

Choose appropriate cluster CIDR's; if you exchange the routes between your HUB and your SPOKE networks (import/export routes), this CIDR's will be visible in other projects and cannot be reused.

## Creation command

~~~bash
RELEASE_CHANNEL="rapid"
CLU_NAME="prod-$RELEASE_CHANNEL-1"
REGION="europe-west3"
PROJECT="$(gcloud config get-value project 2> /dev/null)"
NETWORK_NAME="hub-private-vpc"
GKE_SUBNET_NAME="private-subnet"
GKE_MASTER_CIDR="172.16.0.16/28"
GKE_CLUSTER_CIDR="192.168.64.0/20"
GKE_SERVICES_CIDR="172.17.16.0/20"
 
 
# enable API's
gcloud services enable compute.googleapis.com container.googleapis.com cloudbuild.googleapis.com --project=$PROJECT
 
 
# create cluster
gcloud beta container clusters create $CLU_NAME \
    --no-enable-basic-auth \
    --release-channel $RELEASE_CHANNEL \
    --machine-type "n1-standard-2" \
    --project $PROJECT \
    --region $REGION \
    --network $NETWORK_NAME \
    --subnetwork $GKE_SUBNET_NAME \
    --scopes https://www.googleapis.com/auth/cloud-platform \
    --no-enable-basic-auth \
    --enable-ip-alias \
    --enable-private-nodes \
    --master-ipv4-cidr $GKE_MASTER_CIDR \
    --cluster-ipv4-cidr $GKE_CLUSTER_CIDR \
    --services-ipv4-cidr $GKE_SERVICES_CIDR \
    --no-enable-master-authorized-networks \
    --num-nodes "1" \
    --addons HorizontalPodAutoscaling,HttpLoadBalancing,ApplicationManager \
    --enable-autoupgrade \
    --enable-autorepair \
    --max-surge-upgrade 1 \
    --max-unavailable-upgrade 0 \
    --maintenance-window-start "2020-03-02T19:00:00Z" \
    --maintenance-window-end "2020-03-03T01:00:00Z" \
    --maintenance-window-recurrence "FREQ=WEEKLY;BYDAY=TU,WE" \
    --labels env=prod \
    --enable-stackdriver-kubernetes \
    --security-group "gke-security-groups@sbsit.com" \
    --async
~~~

## On-premises communication

If the cluster should be able to communicate with on-premises services, this communication has to be `masqueraded` with the Kubernetes node ip-address. Per default, this is disabled. Therefore, the 10.0.0.0/8 range is removed. More infos here -> [https://cloud.google.com/kubernetes-engine/docs/how-to/ip-masquerade-agent](https://cloud.google.com/kubernetes-engine/docs/how-to/ip-masquerade-agent).

**Caution: The ip-masq-agent deployment only exists, if you've started a deployment BEFORE.**

~~~bash
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: client
  name: busybox
spec:
  replicas: 1
  selector:
    matchLabels:
      run: client
  template:
    metadata:
      labels:
        run: client
    spec:
      containers:
      - image: busybox
        name: busybox
        command:
          - sleep
          - "3600"
EOF
~~~

~~~bash
tee config << EOF
nonMasqueradeCIDRs:
  - 169.254.0.0/16
  - 172.16.0.0/12
  - 192.168.0.0/16
  - 100.64.0.0/10
  - 192.0.0.0/24
  - 192.0.2.0/24
  - 192.88.99.0/24
  - 198.18.0.0/15
  - 198.51.100.0/24
  - 203.0.113.0/24
  - 240.0.0.0/4
resyncInterval: 60s
masqLinkLocal: true
EOF
kubectl create configmap ip-masq-agent --from-file config --namespace kube-system
~~~

Afterwards SSH connect to an Kubernetes node and check the iptable rules:
~~~bash
iptables -t nat -L IP-MASQ
~~~

And try to ping an on-premises service
~~~
kubectl exec -ti $(kubectl get pod --output=name) -- ping <on-premises-ip>
~~~

Delete test deployment
~~~
kubectl delete deployment busybox
~~~

# Install Istio OSS

Details can be found here -> https://istio.io/docs/setup/platform-setup/gke/

## Prepare installation

First of all, patch the GCP firewall rule to allow the Istio Pilot traffic
~~~bash
GKE_CLU_FW_RULE=$(gcloud compute firewall-rules list --filter="name~gke-$(gcloud container clusters list --format="value(name)")-[0-9a-z]*-master" --format="value(name)" 2> /dev/null)
gcloud compute firewall-rules update $GKE_CLU_FW_RULE --allow tcp:10250,tcp:443,tcp:15017
~~~

Grant some privileges (create cluster role binding)

~~~bash
gcloud container clusters get-credentials $(gcloud container clusters list --format="value(name)") \
  --zone europe-west3

kubectl create clusterrolebinding cluster-admin-binding \
  --clusterrole=cluster-admin \
  --user=$(gcloud config get-value core/account)
~~~

## Install Istio OSS

This will install the latest ISTIO with the `default` profile into the `istio-system` namespace. We **won't touch this later** so this is fine!

Download Istio
~~~bash
curl -L https://istio.io/downloadIstio | sh -
~~~

~~~bash
./istioctl manifest apply
~~~

## Create Gateways

In this demo, the default gateway will be untouched. Because of a bug [https://github.com/istio/istio/issues/22703](https://github.com/istio/istio/issues/22703) all additional Isito gateways are created with **the same name** and excatly **the same labels**! Therefore addtional namesapces are needed.

~~~bash
kubectl create namespace gateway-internal
kubectl create namespace gateway-external
kubectl create namespace gateway-external-restricted
~~~

Create the Istio gateways (Istio operator) with the `istioctl` binary. The internal gateway is using the annotation `cloud.google.com/load-balancer-type: "internal"` which will use an internal VPC ip address. The first one is the default Istio gateway. There will be some changes in the future to this and the `istioctl` binary to ease up the usage!

Save the following file into a file called `gateways.yaml`.

~~~yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  components:
    ingressGateways:
      - name: istio-ingressgateway
        enabled: true
      - namespace: gateway-internal
        name: gateway-internal
        enabled: true
        k8s:
          replicaCount: 2
          resources:
            requests:
              cpu: 200m
          serviceAnnotations:
            cloud.google.com/load-balancer-type: "internal"
          service:
            ports:
            - port: 80
              targetPort: 80
              name: http
            - port: 443
              targetPort: 443
              name: https
      - namespace: gateway-external
        name: gateway-external
        enabled: true
        k8s:
          replicaCount: 2
          resources:
            requests:
              cpu: 200m
          service:
            ports:
            - port: 80
              targetPort: 80
              name: http
            - port: 443
              targetPort: 443
              name: https
      - namespace: gateway-external-restricted
        name: gateway-external-restricted
        enabled: true
        k8s:
          replicaCount: 2
          resources:
            requests:
              cpu: 200m
          service:
            ports:
            - port: 80
              targetPort: 80
              name: http
            - port: 443
              targetPort: 443
              name: https
~~~

Reconfigure the IstioOperator with the previous saved configuration

~~~
./istio-1.5.1/bin/istioctl  manifest apply -f gateways.yaml --set values.global.istioNamespace=istio-system
~~~

Wait a couple of minutes and than check the namespaces for the the running services. Keep in mind, that all services will have the same name (sadly)! But we can workaround this later by patching additional labels to the Istio deployments of the Istio proxies.

Correctly created Kubernetes services, but...
![](gateways-screenshot.jpg)

... the gateways have all the same name. Therefore it is important to use different namespaces!
![](gateways-screenshot-1.jpg)

Next, we will patch the gateways so the can see the originating client ip address. This is essential because otherwise the Isito authorization policies will not work! Details can be obtained here -> https://istio.io/docs/reference/config/security/authorization-policy/

~~~bash
kubectl patch svc gateway-internal -n gateway-internal -p '{"spec":{"externalTrafficPolicy":"Local"}}'
kubectl patch svc gateway-external -n gateway-external -p '{"spec":{"externalTrafficPolicy":"Local"}}'
kubectl patch svc gateway-external-restricted -n gateway-external-restricted -p '{"spec":{"externalTrafficPolicy":"Local"}}'
~~~

Now, at last, we patch the Istio deployment to add individual labels. This makes it possible to them for the appl

~~~bash
kubectl patch deployment gateway-external -n gateway-external --type=json -p='[{"op": "add", "path": "/spec/template/metadata/labels/gateway", "value": "external"}]'
kubectl patch deployment gateway-internal -n gateway-internal --type=json -p='[{"op": "add", "path": "/spec/template/metadata/labels/gateway", "value": "internal"}]'
kubectl patch deployment gateway-external-restricted -n gateway-external-restricted --type=json -p='[{"op": "add", "path": "/spec/template/metadata/labels/gateway", "value": "external-restricted"}]'
~~~

# Run the demo application

The following yaml file does the following in sequential order:

* Create a `namespace` called `demo-mesh` for the application and use it where needed
* Create a `service` and use the selector `run: demo-app`
* Create a `deployment` for the application, use the correct namespace `demo-mesh` and apply the label `run: demo-app`
* Create three Istio gateways, name them correctly eg. `name: gateway-internal` and use the correct selector for them eg. `gateway: external-restricted` - this are the labels, which were patched to the deployments before(!!!)
* Create three Istio virtual services which are using the correct Istio gateways eg. `gateway-external-restricted`. It is possible to only create one virtual service and to use all three gateways at once. This example shows how different application paths of **the same deployment** can be served by different gateways
* Create an Istio AuthorizationPolicy at last, to restrict the appropriate Istio gateways to be only used by allowed ip addresses

~~~yaml
apiVersion: v1
kind: Namespace
metadata:
  name: demo-mesh
  labels:
    istio-injection: enabled
---
apiVersion: v1
kind: Service
metadata:
  name: demo-service
  namespace: demo-mesh
spec:
  ports:
  - port: 80
    name: http
    protocol: TCP
    targetPort: 80
  selector:
    run: demo-app
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: demo-app
  name: demo-app
  namespace: demo-mesh
spec:
  replicas: 1
  selector:
    matchLabels:
      run: demo-app
  template:
    metadata:
      labels:
        run: demo-app
    spec:
      securityContext:
        runAsUser: 0
      containers:
      - image: kennethreitz/httpbin:latest
        name: demo-app
        ports:
        - protocol: TCP
          containerPort: 80
---
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gateway-internal
  namespace: demo-mesh
spec:
  selector:
    gateway: internal # this is important, because it selects the correct ingress Istio pods
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gateway-external-restricted
  namespace: demo-mesh
spec:
  selector:
    gateway: external-restricted
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: gateway-external
  namespace: demo-mesh
spec:
  selector:
    gateway: external
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: demo-mesh-internal
  namespace: demo-mesh
spec:
  hosts:
  - "*"
  gateways:
  - gateway-internal
  http:
  - name: "demo-route-internal"
    match:
    - uri:
        prefix: "/anything/internal"
    route:
    - destination:
        host: demo-service
        port:
          number: 80
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: demo-mesh-external
  namespace: demo-mesh
spec:
  hosts:
  - "*"
  gateways:
  - gateway-external
  http:
  - name: "demo-route-external"
    match:
    - uri:
        prefix: "/anything/external"
    route:
    - destination:
        host: demo-service
        port:
          number: 80
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: demo-mesh-external-restricted
  namespace: demo-mesh
spec:
  hosts:
  - "*"
  gateways:
  - gateway-external-restricted
  http:
  - name: "demo-route-external-restricted"
    match:
    - uri:
        prefix: "/anything/restricted"
    route:
    - destination:
        host: demo-service
        port:
          number: 80
---
apiVersion: security.istio.io/v1beta1
kind: AuthorizationPolicy
metadata:
  name: gateway-external-restricted-ingress-policy
  namespace: gateway-external-restricted
spec:
  selector:
    matchLabels:
      gateway: external-restricted
  action: ALLOW
  rules:
  - from:
    - source:
       ipBlocks: ["80.121.206.135/32", "80.120.254.147/32", "92.60.14.192/27"]
~~~

# Result

## Gateway internal

![](gateway-internal.jpg)

## Gateway external (unrestricted)

![](gateway-external.jpg)

## Gateway external restricted OK

![](gateway-external-restricted.jpg)

## Gateway external restricted blocked

![](gateway-external-restricted-rbac.jpg)

# CI/CD GCP service account user

To use Istio SDS (secure discovery service) for https SSL certificate updates on the Istio gateway it is useful to setup an GCP service account with the appropriate Kubernetes admin permissions.

## Create a service account
~~~bash
gcloud iam service-accounts create gke-sa --display-name "Service account for GKE"
~~~

## Create json key login
~~~bash
PROJECT="$(gcloud config get-value project 2> /dev/null)"
gcloud iam service-accounts keys create .gcp/gke-sa.json --iam-account=gke-sa@$PROJECT.iam.gserviceaccount.com
cloudshell download .gcp/gke-sa.json
~~~

## Add GCP IAM roles to service account user
~~~bash
PROJECT="$(gcloud config get-value project 2> /dev/null)"
for role in \
    'roles/container.admin'
do \
    gcloud projects add-iam-policy-binding \
        $PROJECT \
        --member=serviceAccount:gke-sa@$PROJECT.iam.gserviceaccount.com \
        --role="${role}"
done
~~~

## Login as service account
This step is optional and only used, if you need to add a SSH key pair to the use. See [this link for details.](https://alex.dzyoba.com/blog/gcp-ansible-service-account/)
~~~bash
# Login as sa
gcloud auth activate-service-account \
    --key-file=.gcp/gke-sa.json

# Configure public key
gcloud compute os-login ssh-keys add \
    --key-file=ssh-key-gke-sa.pub

# Check out who you are
gcloud config get-value core/account

# switch back to your user
gcloud config set account your@gmail.com
~~~

## Add Kubernetes permissions to service account
~~~bash
PROJECT="$(gcloud config get-value project 2> /dev/null)"
kubectl create clusterrolebinding gke-sa-admin-binding \
    --clusterrole=cluster-admin \
    --user=gke-sa@$PROJECT.iam.gserviceaccount.com
~~~

Afterwards login as `gke-sa` user and try out, if you can access the kube-system namespace. From now on, you can use the `gke-sa.json` for the `gcloud login` and then do your `kubectl` stuff.

~~~bash
PROJECT="$(gcloud config get-value project 2> /dev/null)"
gcloud container clusters get-credentials prod-rapid-1 --region europe-west3 --project $PROJECT
~~~

# Istio SDS (secure service discovery)

This is used to apply SSL certificates to the Istio Gateways and/or to the mTLS encryption. The benefit of the system is, that the Istio gateway is reloading the gateway service (not restarting!) every time the certificate is updated. Especially this is the case when you are using LetsEncrypt. The drawback is, that if you are managing a lot of LetsEncrypt certificates, like we do (>500), you probably have to create a lot of Kubernetes secrets. And furthermore, if you have such a large amount of certificates, you are already renewing them in a central place. Therefore some kind of glue between your CI/CD and Istio is needed to handle this.

We are using GitLab. Therefore, this part is mostly GitLab specific, but you can use the information for other systems too.

# Conclusion

This setup allows you to use one application deployment with different
* Istio routing paths
* over different gateways
* over different networks (internal, external, restricted)
* with different authorization policies
* at the same time

The cost of this flexible setup is, that you have to do some additional preparations because there is a bug inside Istio and all gateways are created with the same gateway names and labels. But this might be changed in the future.
